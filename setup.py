from setuptools import setup, find_packages

setup(
    name='subs',
    install_requires=[
        'openai-whisper @ git+https://github.com/openai/whisper.git',
        'click==8.1.3',
    ],
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'subs = subs.__main__:main'
        ]
    }
)
